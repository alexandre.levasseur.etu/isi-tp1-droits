#!/bin/bash

groupadd group_a
groupadd group_b

adduser lambda_a
adduser lambda_b


usermod -a -G group_a lambda_a
usermod -a -G group_b lambda_b
