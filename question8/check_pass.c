#include<stdio.h>
#include<unistd.h>
#include<stdlib.h>
#include<string.h>
#include "check_pass.h"

int check_pass(int id, char *pass){
    char *line=NULL;
    char *password= NULL;
    char *userId= NULL;

    size_t len = 0;
    FILE *fpassword;
    fpassword = fopen("/home/admin/passwd", "r");

    if (fpassword == NULL){
        printf("File doesn't exist\n");
    }

    else{
        while ((getline(&line, &len, fpassword) != -1)) {
            userId = strtok(line, ":");
            if(atoi(userId) == id){
                password = strtok(NULL,":");
	            password = strtok(password,"\n");
                fclose(fpassword);
                return strcmp(password,pass);
            }
        }
        
    }

    fclose(fpassword);
    return 1;

}


