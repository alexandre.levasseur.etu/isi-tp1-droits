# Rendu "Les droits d’accès dans les systèmes UNIX"

## Binome

- Almosaalmaksour, Ahmad, email: ahmad.almosaalmaksour.etu@univ-lille.fr

- Levasseur, Alexandre, email: alexandre.levasseur.etu@univ-lille.fr

## Question 1

Oui le processus peut écrire car l’utilisateur toto appartient au groupe ubuntu et ubuntu a le droit de lire et écrire sur le fichier myfile.txt.

## Question 2

- Le caractère x signifie qu'on peut accéder au répertoire.
- enlever le droit d’exécution au groupe ubuntu: chmod g-x mydir
- quand j'essaie d'acceder au mydir avec l'utilisateur toto ça me donne : Permission denied, car on a plus le droit d'acceder au mydir avec les utilisateur qui apartienne au groupe ubunutu.
- On a listé le contenu du mydir et ça affiche : 
	ls: connot access 'mydir/.' : Permission denied
	ls: connot access 'mydir/..' : Permission denied
	ls: connot access 'mydir/data.txt' : Permission denied
	total 0 
	d?????????????????????                ?.
	d?????????????????????                ?..
	d?????????????????????                ? data.txt
On arrive à afficher le contenu du mydir mais sans voir les droits de permission car on a plus le droit d'acceder au mydir avec les utilisateur qui apartienne au groupe ubunutu.

## Question 3

ruid= 1001, rgid= 1001
euid= 1001, egid= 1001
Connot open file : Permission denied
le processeur n'a pas pu ouvrir data.txt puisque il n'a pas le droit d'acceder au mydir.


$chmod u+s suid

ruid= 1002, rgid= 1002
euid= 1001, egid= 1002
file opens correctly

comme toto prends la propriété d'utilsateur ubuntu avec set_id(), le processus a reussi à ouvrir mydir/data.txt en mode lecture.


## Question 4

- euid : 1001
  egid : 1002

## Question 5

chfn : changer les information d'un utilisatur.

-rwsr-xr-x 1 root root 85064 mai 28  2020 /usr/bin/chfn

-l’administrateur root a le droit de lire et écrir et le flag set-user-id est activé
-les utilisateur qui apartienne au groupe root ont le droit de lire et d'éxecution mais pas d'écrire.
-les autre utilisatuers ont le droit que pour l'éxecution.

## Question 6

Puisque le mot de pass doit être encrypté, on ne peut pas visualiser.

Les mots de pass sont stocké dans etc/shadow et seul l’administrateur root a l'accès aux mots de pass.


## Question 7

Mettre les scripts bash dans le repertoire *question7*.

## Question 8

Le programme et les scripts dans le repertoire *question8*.

## Question 9

Le programme et les scripts dans le repertoire *question9*.

## Question 10

Les programmes *groupe_server* et *groupe_client* dans le repertoire
*question10* ainsi que les tests. 








