#include <stdio.h>
#include <stdlib.h>

int main()
{
	
	FILE *f;

	printf("ruid=%d, rgid=%d\n", getuid(), getgid());

	printf("euid=%d, egid=%d\n", geteuid(), getegid());

	f = fopen("./mydir/mydata.txt", "r");

	if (f == NULL) {
		perror("Cannot open file");
		exit(EXIT_FAILURE);
	}
	printf("File opens correctly\n");
	fclose(f);

	exit(EXIT_SUCCESS);
}
